# Docker image with Android SDK and Flutter

Based on [`openjdk:8-slim`](https://hub.docker.com/_/openjdk/) and the work of
[�lvaro S.](https://github.com/alvr/alpine-android) & [valotas](https://github.com/valotas/flutter-android-sdk) on installing the Android
SDK.

## Building and running

```
docker build -t [image-name] .
docker run -it --entrypoint=/bin/bash [image-name]
```

## Bitbucket Pipelines

This image can be used to test and build Android APKs developed with Flutter.
Here is an example `bitbucket-pipelines.yml` for Bitbucket Pipelines including
caches for gradle and gradlewrapper:

```
image: clemax/flutter-android-sdk:latest

pipelines:
  default:
    - step:
        caches:
          - gradle
          - gradlewrapper
          - flutter
        script:
          - echo "Building APK..."
          - flutter upgrade
          - flutter doctor
          - flutter -v build apk

definitions:
  caches:
    gradlewrapper: ~/.gradle/wrapper
    flutter: /opt/flutter
```
## SDK Licenses
By using this container you agree to the Android SDK Licenses.
